export const getDataTracking = async (trackingNo,courier) => {
    try {
    const bodyData = "{\"trackingNo\": \""+trackingNo+"\",\"courier\":\""+courier+"\"}";
    const response = await fetch('https://fast.etrackings.com/api/v3/tracks/find',{
         method:"POST",
         headers:{
             'content-type':'application/json',
             'Accept-Language':'TH',
             'Etrackings-Api-Key':'164d03074c7c717a8555ba6af08db9c03efe68c7',
             'Etrackings-Key-Secret':'0c3260a2cda782cb2f12818978c3cc5e0fa1b800b37c8bad73c8583901e2d5f3e841f17a3a30ee782e9e2bdc2d2f20b5692c74a541f7eb93174ead43395271ef48a6e1d0dfe3c67439cfe3'
         },
         body:bodyData
     });
     const json = await response.json();
     return json;
   } catch (error) {
     console.error(error);
   }
 }