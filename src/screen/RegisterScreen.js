import React, {useState, useEffect} from 'react'
import { View, Text, Alert, TextInput, TouchableOpacity, ImageBackground, StyleSheet, Dimensions,Image  } from 'react-native'
import AsyncStorage from '@react-native-async-storage/async-storage'
const { width, height } = Dimensions.get("window");
export default function RegisterScreen({ navigation }) {

    const [isLoading,setisLoading] = useState(false);
    const [password,setpassword] = useState('');

    useEffect(() => {
        try {
            setisLoading(true);

            setisLoading(false);
        } catch (error) {
          Alert.alert("Error!",error.toString());
        }
      }, []);

      const setDataStore = async () => {
        try {
            //รหัสนักเรียน
            await AsyncStorage.setItem('password', password);
            //ถ้า login มาแล้วเช็ต FirstLogin เป็น false แล้วไม่ต้องมา register ใหม่
            await AsyncStorage.setItem('FirstLogin', "false");
            navigation.reset({
                index: 0,
                routes: [{ name: 'LoginScreen' }],
              });
              
        } catch (error) {
            Alert.alert("Cannot save data to AsyncStorage",error.toString());
        }
    }

    const inputPasswordChange = (val) => {
        try {
            setpassword(val)
        } catch (error) {
            Alert.alert("Error", error)
        }
    }
   const nahee =  {uri :"https://image.freepik.com/free-photo/business-lady-suit-with-glasses-holding-her-gift-saying-bye-sitting-table-with-xsmas-tree-it-office_179666-20017.jpg"}
    return (
        <View>
            {
            isLoading == true ? null :
            <View style={styles.container}>
                
                <ImageBackground source={require('../img/01.png')} style={styles.image}>
                <View style={{backgroundColor:'rgba(52, 52, 52, 0.4)',alignItems: 'center',justifyContent:'center'}}>
                <Text style={[styles.text,{alignItems: 'center',alignSelf:'center',fontSize: 30}]}>RegisterScreen</Text>
                </View>
                <View style={{backgroundColor:'rgba(52, 52, 52, 0.4)',height:height,alignItems: 'center',justifyContent:'center'}}>
                  <TextInput
                   style={[styles.textlogin,{marginTop:20,backgroundColor:'#999999',borderWidth:1,borderRadius:20}]}
                  value={password}
                  onChangeText={(val) => inputPasswordChange(val)}
                  placeholder={'password'}
                  keyboardType={'number-pad'}/>
                <TouchableOpacity onPress={() => setDataStore()}
                style={[styles.background,{height: height * 0.1,width: width * 0.5,alignSelf:'center',alignItems: 'center',
                backgroundColor:'#ffdd61',marginTop:90
                }]}
                >
                    <View>
                        <Text style={styles.text}>สมัคร</Text>
                    </View>
                </TouchableOpacity>
                </View>
                </ImageBackground>
            </View>
            }
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
      // justifyContent: 'center',
      // alignItems: 'center',
      width: '100%',
      height: '100%',
      
    },
    item: {
     backgroundColor:'rgba(100, 100, 100, 0.8)',
      padding: 30,
      marginVertical: 5,
      marginHorizontal: 16
    },
    image: {
      flex: 1,
      // justifyContent: "center"
    },
    text: {
      color: "black",
      fontSize: 18,
      lineHeight: 72,
      fontWeight: "bold",
    //   textAlign: "center",
      // backgroundColor: "#CCFFCCbb",
    },
    background: {
        width: width,
        height: height * 0.25,
        backgroundColor: "#788995",
        justifyContent: "center",
      },
      imgbackground: {
        width: width,
        height: height * 0.25,
        position: "absolute",
        zIndex: 99,
      },
      textlogin: {
        color: "black",
        fontSize: 18,
        alignSelf: "center",
        borderColor: "black",
        borderWidth:0.3,
        width:width*0.6,
        paddingStart:30
      },
  });
