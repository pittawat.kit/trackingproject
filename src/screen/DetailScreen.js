import AsyncStorage from '@react-native-async-storage/async-storage';
import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  Alert,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  StatusBar, ImageBackground, Dimensions, Image
} from 'react-native';
import appsetting from '../appsetting.json';
const { width, height } = Dimensions.get("window");
export default function DetailScreen({ route, navigation }) {
  const [isLoading, setisLoading] = useState(false);
  const [data, setdata] = useState({}); //ก้อนใหญ่
  const [timeline, settimeline] = useState([]); //ก้อน timeline
  const [detail, setdetail] = useState([]); //ก้อน detail
  const [detail2, setdetail2] = useState([]); //ก้อน detail

  const [trackingNo, settrackingNo] = useState(route.params.trackingNo);
  const [courier, setcourier] = useState(route.params.courier);


  useEffect(() => {
    setisLoading(true);
    GetdataFromService();
    setisLoading(false);
  }, []);

  const GetdataFromService = async () => {
    try {
      console.log(`trackingNo`, trackingNo)
      console.log(`courier`, courier)
      const mock = appsetting;
      if (mock.useMock) { //ใช้ mockData จาก appsetting.json
        setdata(mock.dataTest);
        settimeline(mock.dataTest.data.timelines);
        setdetail(mock.dataTest.data.timelines[0].details)
        setdetail2(mock.dataTest.data.timelines[1].details)
      } else {
        const bodyData = "{\"trackingNo\": \"" + trackingNo + "\",\"courier\":\"" + courier + "\"}";
        const response = await fetch('https://fast.etrackings.com/api/v3/tracks/find', {
          method: "POST",
          headers: {
            'content-type': 'application/json',
            'Accept-Language': 'TH',
            'Etrackings-Api-Key': appsetting.EtrackingsApiKey,
            'Etrackings-Key-Secret': appsetting.EtrackingsKeySecret
          },
          body: bodyData
        });
        const json = await response.json();
        console.log(`json`, JSON.stringify(json))
        if (json.meta.code.toString() == '200') {
          setdata(json);
          settimeline(json.data.timelines);
          setdetail(json.data.timelines[0].details)
        } else {
          Alert.alert("หมายเหตุ", "ขออภัย ไม่พบข้อมูลเลขพัสดุนี้", [
            {
              text: "ตกลง",
              onPress: async () => {
                var oData = await AsyncStorage.getItem('TrackingList');
                var arroData = JSON.parse(oData);
                var mData = {
                  "courier": courier,
                  "trackingNo": trackingNo
                };
                var filtered = arroData.filter(function (value, index, arr) {
                  return value.trackingNo != trackingNo;
                });
                await AsyncStorage.removeItem('TrackingList');
                await AsyncStorage.setItem('TrackingList', JSON.stringify(filtered));
                navigation.reset({
                  index: 0,
                  routes: [{ name: 'MainScreen' }],
                });

              }
            }
          ])
        }
      }
    } catch (error) {
      Alert.alert('Error! GetdataFromService', error.toString());
    }
  };

  const Item = ({ title }) => (
    <View>
      <View
        style={{
          width: '100%',
          height: 500,
          backgroundColor: '#F2E135dd',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 10,
          padding: 30
        }}>
        <Text style={{ color: 'black', fontSize: 18 }}>วันที่ : {title.date}</Text>
        {/* <Text style={{ color: 'black', fontSize: 12 }}>{title.description}</Text> */}
        <FlatList
        data={title.details}
        renderItem={ e => {
          return(
            <View>
              <Text>{e.item.description}</Text>
            </View>
          )
        }}
        />
      </View>
    </View>
  );


  const renderItem = ({ item }) => <Item title={item} />;

  return (
    <View
      style={{
        backgroundColor: 'black',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {isLoading == true ? null : (
        <View
          style={{
            backgroundColor: 'black',
            width: '100%',
            height: '100%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>

          <ImageBackground source={require('../img/02.png')} style={styles.image}>
            <View style={{ alignItems: 'center' }}>
              <View style={{ backgroundColor: 'rgba(52, 52, 52, 0.4)', width: width, height: height * 0.98, alignItems: 'center', justifyContent: 'center' }}>
                <Text
                  style={{
                    color: 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                    fontSize: 20,
                    marginTop: 20
                  }}>
                  เลขที่พัสดุ : {trackingNo}
                </Text>
                <Text
                  style={{
                    color: 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                    fontSize: 18,
                    marginBottom: 12
                  }}>
                  ขนส่ง : {courier}
                </Text>
                <FlatList
                  data={timeline}
                  renderItem={renderItem}
                />

                <TouchableOpacity style={{ backgroundColor: '#343001', width: '100%' }} onPress={() => navigation.goBack()}>
                  <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ color: 'white', fontSize: 26, padding: 10 }}>Back</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </ImageBackground>
        </View>
      )}
    </View>
  );
}
const styles = StyleSheet.create({

  container: {
    // justifyContent: 'center',
    // alignItems: 'center',
    width: '100%',
    height: '100%',

  },
  item: {
    backgroundColor: 'rgba(100, 100, 100, 0.8)',
    padding: 30,
    marginVertical: 5,
    marginHorizontal: 16
  },
  image: {
    flex: 1,
    // justifyContent: "center"
  },
  text: {
    color: "black",
    fontSize: 18,
    lineHeight: 72,
    fontWeight: "bold",
    //   textAlign: "center",
    // backgroundColor: "#CCFFCCbb",
  },
  background: {
    width: width,
    height: height * 0.25,
    backgroundColor: "#788995",
    justifyContent: "center",
  },
  imgbackground: {
    width: width,
    height: height * 0.25,
    position: "absolute",
    zIndex: 99,
  },
  textlogin: {
    color: "black",
    fontSize: 18,
    alignSelf: "center",
    borderColor: "black",
    borderWidth: 0.3,
    width: width * 0.6,
    paddingStart: 30
  },
});
