import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Alert,
  FlatList,
  SafeAreaView,
  StyleSheet,
  Modal,
  Pressable,
  TextInput,
  Button, ImageBackground, Dimensions,Image
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
const { width, height } = Dimensions.get("window");
export default function MainScreen({ navigation }) {
  const [isLoading, setisLoading] = useState(false);
  const [TrackingList, setTrackingList] = useState([]);
  const [selectedId, setSelectedId] = useState(null);

  const [newTrackingNo, setnewTrackingNo] = useState('');
  const [newcourier, setnewcourier] = useState('');

  const [modalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    setisLoading(true);
    getData();
    setisLoading(false);
  }, []);

  const getData = async () => {
    try {
      const trackingNo = await AsyncStorage.getItem('TrackingList');
      console.log(JSON.parse(trackingNo));
      setTrackingList(JSON.parse(trackingNo));
    } catch (error) {
      Alert.alert('Error! getDataFromStore', error.toString());
    }
  };

  const Item = ({ item, onPress, backgroundColor, textColor }) => (
    <TouchableOpacity
      onPress={() =>
        navigation.navigate('DetailScreen', {
          trackingNo: item.trackingNo,
          courier: item.courier,
        })
      }
      style={{
        width: 300,
        height: 100,
        backgroundColor: '#ffdd61dd',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10,
      }}>
      <Text style={[styles.title, textColor]}>{item.trackingNo}</Text>
    </TouchableOpacity>
  );

  const renderItem = ({ item }) => {
    const backgroundColor = 'pink';
    const color = 'white';

    return (
      <Item
        item={item}
        backgroundColor={{ backgroundColor }}
        textColor={{ color }}
      />
    );
  };

  const setstatenewTrackingNo = (val) => {
    setnewTrackingNo(val)
  }
  const setstatenewcourier = (val) => {
    setnewcourier(val)
  }
  const submit = async () => {
    try {

      setModalVisible(!modalVisible)
      var newItem = {
        'trackingNo': newTrackingNo,
        'courier': newcourier
      };
      var arrTrackingList = [];
      if (TrackingList == null) {
        arrTrackingList.push(newItem);
        setTrackingList(arrTrackingList);
        await AsyncStorage.setItem('TrackingList', JSON.stringify(TrackingList))
      } else {
        TrackingList.push(newItem);
        await AsyncStorage.removeItem('TrackingList')
        await AsyncStorage.setItem('TrackingList', JSON.stringify(TrackingList))
        setTrackingList(TrackingList);
      }
      setnewTrackingNo('');
    } catch (error) {
      Alert.alert("error! submit", error.toString());
    }
  }

  const pressJt = async () =>{
    try {
      
      setModalVisible(!modalVisible)
      var newItem = {
        'trackingNo': newTrackingNo,
        'courier': 'jt-express'
      };
      var arrTrackingList = [];
      if (TrackingList == null) {
        arrTrackingList.push(newItem);
        setTrackingList(arrTrackingList);
        await AsyncStorage.setItem('TrackingList', JSON.stringify(TrackingList))
      } else {
        TrackingList.push(newItem);
        await AsyncStorage.removeItem('TrackingList')
        await AsyncStorage.setItem('TrackingList', JSON.stringify(TrackingList))
        setTrackingList(TrackingList);
      }
      setnewTrackingNo('');
    } catch (error) {
      Alert.alert("error! pressJt", error.toString());
    }
  }

  const pressKerry = async () =>{
    try {
      
      setModalVisible(!modalVisible)
      var newItem = {
        'trackingNo': newTrackingNo,
        'courier': 'kerry-express'
      };
      var arrTrackingList = [];
      if (TrackingList == null) {
        arrTrackingList.push(newItem);
        setTrackingList(arrTrackingList);
        await AsyncStorage.setItem('TrackingList', JSON.stringify(TrackingList))
      } else {
        TrackingList.push(newItem);
        await AsyncStorage.removeItem('TrackingList')
        await AsyncStorage.setItem('TrackingList', JSON.stringify(TrackingList))
        setTrackingList(TrackingList);
      }
      setnewTrackingNo('');
    } catch (error) {
      Alert.alert("error! pressJt", error.toString());
    }
  }

  const pressShopee = async () =>{
    try {
      
      setModalVisible(!modalVisible)
      var newItem = {
        'trackingNo': newTrackingNo,
        'courier': 'shopee-express'
      };
      var arrTrackingList = [];
      if (TrackingList == null) {
        arrTrackingList.push(newItem);
        setTrackingList(arrTrackingList);
        await AsyncStorage.setItem('TrackingList', JSON.stringify(TrackingList))
      } else {
        TrackingList.push(newItem);
        await AsyncStorage.removeItem('TrackingList')
        await AsyncStorage.setItem('TrackingList', JSON.stringify(TrackingList))
        setTrackingList(TrackingList);
      }
      setnewTrackingNo('');
    } catch (error) {
      Alert.alert("error! pressJt", error.toString());
    }
  }

  const pressFlash = async () =>{
    try {
      
      setModalVisible(!modalVisible)
      var newItem = {
        'trackingNo': newTrackingNo,
        'courier': 'flash-express'
      };
      var arrTrackingList = [];
      if (TrackingList == null) {
        arrTrackingList.push(newItem);
        setTrackingList(arrTrackingList);
        await AsyncStorage.setItem('TrackingList', JSON.stringify(TrackingList))
      } else {
        TrackingList.push(newItem);
        await AsyncStorage.removeItem('TrackingList')
        await AsyncStorage.setItem('TrackingList', JSON.stringify(TrackingList))
        setTrackingList(TrackingList);
      }
      setnewTrackingNo('');
    } catch (error) {
      Alert.alert("error! pressJt", error.toString());
    }
  }

  const pressDhl = async () =>{
    try {
      
      setModalVisible(!modalVisible)
      var newItem = {
        'trackingNo': newTrackingNo,
        'courier': 'dhl-express'
      };
      var arrTrackingList = [];
      if (TrackingList == null) {
        arrTrackingList.push(newItem);
        setTrackingList(arrTrackingList);
        await AsyncStorage.setItem('TrackingList', JSON.stringify(TrackingList))
      } else {
        TrackingList.push(newItem);
        await AsyncStorage.removeItem('TrackingList')
        await AsyncStorage.setItem('TrackingList', JSON.stringify(TrackingList))
        setTrackingList(TrackingList);
      }
      setnewTrackingNo('');
    } catch (error) {
      Alert.alert("error! pressJt", error.toString());
    }
  }

  return (
    <SafeAreaView>
      {isLoading == true ? null : (
        <View
          style={styles.container}>
            <ImageBackground source={require('../img/01.png')} style={styles.image}>
          <View style={{alignItems: 'center'}}>
          <View style={{backgroundColor:'rgba(52, 52, 52, 0.4)',width: width,height: height*0.1,alignItems: 'center',justifyContent:'center'}}>
            
            <TouchableOpacity onPress={() => setModalVisible(true)}>
              <View>
                <Text style={{ color: 'green', fontWeight: 'bold',fontSize:20}}>
                  เพิ่มเลขขนส่ง
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity onPress={async () => {
              Alert.alert("คำเตือน", "ต้องการลบข้อมูลทั้งหมดใช่หรือไม่", [
                {
                  text: "ใช่",
                  onPress: async () => {
                    await AsyncStorage.removeItem('TrackingList')
                    setTrackingList([]);
                  }
                },
                {
                  text: "ไม่",
                  onPress: () => console.log("Cancel Pressed"),
                  style: "cancel"
                }
              ])

            }}>
              <View>
                <Text style={{ color: 'red', fontWeight: 'bold',fontSize:20 }}>
                  เคลียข้อมูลเลขขนส่ง
                </Text>
              </View>
            </TouchableOpacity>

            </View>
            <View style={{backgroundColor:'rgba(52, 52, 52, 0.4)',height:height*0.88,width:width,alignItems: 'center',justifyContent:'center'}}>
            <FlatList
              data={TrackingList}
              renderItem={renderItem}
              keyExtractor={item => item.trackingNo}
              extraData={selectedId}
            />
            </View>
            <Modal
              animationType="slide"
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                Alert.alert('Modal has been closed.');
                setModalVisible(!modalVisible);
              }}>
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text style={styles.modalText}>กรุณาใส่เลขขนส่ง</Text>
                  <TextInput
                  style={{borderWidth:1}}
                    value={newTrackingNo}
                    onChangeText={val => setstatenewTrackingNo(val)}
                    placeholder={'กรอกเลขขนส่ง'}
                  />
                  {/* <TextInput
                    value={newcourier}
                    onChangeText={val => setstatenewcourier(val)}
                    placeholder={'กรอกรหัสขนส่ง'}
                  />
                  <Pressable
                    style={[styles.button, styles.buttonClose]}
                    onPress={() => submit()}>
                    <Text style={styles.textStyle}>บันทึกข้อมูล</Text>
                  </Pressable> */}
                  <View style={{marginTop:15}}></View>
                  <Button
                    title="jt-express"
                    color="#FE1B3A"
                    onPress={() => pressJt()}
                  />
                  <View style={{marginTop:10}}></View>
                  <Button
                    title="kerry-express"
                    color="#FB8523"
                    onPress={() => pressKerry()}
                  />
                  <View style={{marginTop:10}}></View>
                  <Button
                    title="shopee-express"
                    color="#FF8C00"
                    onPress={() => pressShopee()}
                  />
                  <View style={{marginTop:10}}></View>
                  <Button
                    title="flash-express"
                    color="#FFD700"
                    onPress={() => pressFlash()}
                  />
                  <View style={{marginTop:10}}></View>
                  <Button
                    title="dhl-express"
                    color="#FF6347"
                    onPress={() => pressDhl()}
                  />
                  
                </View>
              </View>
            </Modal>
          </View>
          </ImageBackground>
        </View>
      )}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
    fontSize:20,
    fontWeight: 'bold'
  },
  container: {
    // justifyContent: 'center',
    // alignItems: 'center',
    width: '100%',
    height: '100%',
    
  },
  item: {
   backgroundColor:'rgba(100, 100, 100, 0.8)',
    padding: 30,
    marginVertical: 5,
    marginHorizontal: 16
  },
  image: {
    flex: 1,
    // justifyContent: "center"
  },
  text: {
    color: "black",
    fontSize: 18,
    lineHeight: 72,
    fontWeight: "bold",
  //   textAlign: "center",
    // backgroundColor: "#CCFFCCbb",
  },
  background: {
      width: width,
      height: height * 0.25,
      backgroundColor: "#788995",
      justifyContent: "center",
    },
    imgbackground: {
      width: width,
      height: height * 0.25,
      position: "absolute",
      zIndex: 99,
    },
    textlogin: {
      color: "black",
      fontSize: 18,
      alignSelf: "center",
      borderColor: "black",
      borderWidth:0.3,
      width:width*0.6,
      paddingStart:30
    },
});